window.onload = init;
var x = 400;
var y = 100;
var up = false;
var right = false;
var left = false;
var gravity = 0;
var horizontalSpeed = 0.1;
var ctx;
var fuel = 190;
var hack = false;

function init(){
	document.onkeydown = handleKeyDown;
	document.onkeyup = handleKeyUp;

	var canvas = document.querySelector("canvas");
	ctx  = canvas.getContext("2d");
	animate();
}
function handleKeyDown(evt) {
	evt = evt || window.event;
	switch (evt.keyCode) {
		case 37: 	//arrow left 
			left = true;
			break;
		case 38: 		//arrow up
			up = true;
			break;
		case 39: 		//arrow right
			right = true;
			break;
		case 45:  		//insert key
			hack = true; 
			break;
	}
}
function handleKeyUp(evt) {
evt = evt ||window.event;
	switch (evt.keyCode) {
		case 37:       //arrow left 
			left = false;
			break;
		case 38:   //arrow up
			up = false;
			break;
		case 39:	//arrow right
			right = false;
			break;
		case 45:
			hack = false; //insert key
			break;
	}
}

function animate(){

	moveShip();
	drawScene();
	requestAnimationFrame(animate);
	borders();
	//drawBlocks();
}

function borders(){
	if (x < -20){
		x = 820;
	};
	if (x > 820){
		x = -20;
	}

	if (y < 0){
		y = 0;
		gravity = 0;
	} else if (y > 460){
		y = 460;
		
		if (gravity >= 3)
		   {

		 	//explosion
		 	ctx.save();
			ctx.translate(x, y);
			ctx.beginPath();
			ctx.strokeStyle = "red"
			ctx.fillStyle ="orange"
			ctx.moveTo(10,10);	
			ctx.arc(0,40,80,0,Math.PI*2,false);
			ctx.stroke();
			ctx.closePath();
			ctx.fill();

			//game over
		 	alert("GAME OVER! press OK to restart");
			location.reload();
		 }
		 var score=fuel;
		 if (gravity <=2.9){
		alert('Good job! your score is: '+score+' press OK to restart');
		 	location.reload();

		 }
		}
	}


function moveShip(){
	gravity -= 0;
	if (left){
		horizontalSpeed -= 0.1;
	} else if (right){
		horizontalSpeed += 0.1;
	}
	if (up) {
		gravity -= 0.4;
		fuel -= 1;
	}
	if (hack){
		fuel = 9999;}
	gravity += 0.12;
	y += gravity;
	x += horizontalSpeed;
}
function drawScene() {
	ctx.clearRect(0,0,800,640);
	drawSpaceship();
	ctx.font = "30px Arial"
	ctx.fillStyle="red"
	ctx.fillText('fuel:  ' + fuel,720,30,50)

}	
//function drawBlocks(){
//	ctx.save();
//		ctx.beginPath();
//			ctx.fillStyle ="yellow";
//			ctx.strokeStyle ="red";
//			ctx.fillRect(400,400,40,40);	
//			ctx.stroke();
//			ctx.closePath();
//			ctx.fill();
//	ctx.save();
//			ctx.beginPath();
//			ctx.fillStyle ="yellow";
//         	ctx.strokeStyle ="red";
//			ctx.fillRect(500,400,40,40);	
//			ctx.stroke();
//			ctx.closePath();
//			ctx.fill();		

//}

function drawSpaceship(){

	ctx.save();
	ctx.translate(x, y);
	ctx.beginPath();

		
			//top of the spaceship
		ctx.fillStyle='red';
		ctx.strokeStyle='red';
		ctx.moveTo(0, -10);
		ctx.lineTo(10, 20);
		ctx.lineTo(-10, 20);
		ctx.lineTo(0, -10);	
		ctx.stroke();
		ctx.fill();
		ctx.closePath();
		
		//tail
		ctx.beginPath();
		ctx.fillStyle='red';
		ctx.moveTo(0, 10);
		ctx.lineTo(-20, 78);
		ctx.lineTo(20, 78);
		ctx.fill();
		ctx.closePath();


			//mid block
		ctx.beginPath();
		ctx.fillStyle = "grey";
		ctx.fillRect(-10,20,20,60);	
		ctx.stroke();
		ctx.closePath();

			//window

		ctx.fillStyle='#ADD8E6';
		ctx.beginPath();
		ctx.moveTo(10,10);
		ctx.arc(0,40,8,0,Math.PI*2,false);
		ctx.closePath();
		ctx.fill();
	
		
//fire
if (up) {
 ctx.beginPath();
 ctx.moveTo(-9, 79);
 ctx.lineTo(-7, 82);
 ctx.lineTo(-3, 90);
 ctx.lineTo(3, 96);
 ctx.lineTo(7, 91);
 ctx.lineTo(9, 85);
 ctx.lineTo(10, 80);
 ctx.lineTo(10, 80);
 ctx.stroke();
 ctx.fillStyle = "orange";
 ctx.fill();
 ctx.closePath();

};ctx.restore();

//fuel error
if (fuel == 0){
	alert("out of fuel press OK to restart");
	location.reload();
	}
}		
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());